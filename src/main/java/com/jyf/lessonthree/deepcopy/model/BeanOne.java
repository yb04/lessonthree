package com.jyf.lessonthree.deepcopy.model;

import lombok.Data;

@Data
public class BeanOne {
    private String id;
    private String name;
    private String beanOne;
}
