package com.jyf.lessonthree.deepcopy.model;

import lombok.Data;

@Data
public class BeanTwo {
    private String id;
    private String name;
    private String beanTwo;
}
