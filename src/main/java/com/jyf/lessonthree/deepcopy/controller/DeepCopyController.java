package com.jyf.lessonthree.deepcopy.controller;

import com.jyf.lessonthree.deepcopy.model.BeanOne;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeepCopyController {

    @GetMapping("/deepcopy")
    public  void deepcopytest(){
        BeanOne orig = new BeanOne();
        orig.setId("123");
        orig.setId("张三");
        orig.setBeanOne("one");
//直接赋值
        BeanOne dest = orig;
//修改值
        dest.setId("456");
        dest.setName("李四");
        System.out.println("赋值后的原始值："+orig.toString());
        System.out.println("赋值后新对象值："+dest.toString());
    }
}
