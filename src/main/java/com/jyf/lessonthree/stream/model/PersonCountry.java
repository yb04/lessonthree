package com.jyf.lessonthree.stream.model;

import lombok.Data;

@Data
public class PersonCountry {

    private String country;
}
