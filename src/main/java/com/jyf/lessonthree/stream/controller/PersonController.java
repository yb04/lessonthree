package com.jyf.lessonthree.stream.controller;

import com.jyf.lessonthree.stream.model.Person;
import com.jyf.lessonthree.stream.model.PersonCountry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PersonController {
    @GetMapping("/testStream01/{id}")
    public void testStream01(){
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("欧阳雪",18,"中国",'F'));
        personList.add(new Person("Tom",24,"美国",'M'));
        personList.add(new Person("Harley",22,"英国",'F'));
        personList.add(new Person("向天笑",20,"中国",'M'));
        personList.add(new Person("李康",22,"中国",'M'));
        personList.add(new Person("小梅",20,"中国",'F'));
        personList.add(new Person("何雪",21,"中国",'F'));
        personList.add(new Person("李康",22,"中国",'M'));
        // 1）找到年龄大于18岁的人并输出；
        System.out.println("找到年龄大于18岁的人并输出");
        personList.stream()
                .filter((p) ->p.getAge() > 18)
                .forEach(System.out::println);
        System.out.println("**********************************");
        // 2）找出所有中国人的数量
        Long chinaPersonNum=personList.stream()
                .filter((p)->p.getCountry().equals("中国"))
                .count();
        System.out.println("中国人数"+chinaPersonNum+"个");
        System.out.println("*************************************");
        //Person列表中取出两个女性。
        System.out.println("Person列表中取出两个女性");
        personList.stream()
                .filter(p ->p.getSex()=='F')
                .limit(2)
                .forEach(System.out::println);
        System.out.println("****************************************");
        //从Person列表中从第2个女性开始，取出所有的女性。
        System.out.println("从Person列表中从第2个女性开始，取出所有的女性。");
        personList.stream()
                .filter(p->p.getSex()=='F')
                .skip(2)
                .forEach(System.out::println);
        System.out.println("********************************************");
        //用一个PersonCountry类来接收所有的国家信息：
        System.out.println("用一个PersonCountry类来接收所有的国家信息：");
        personList.stream()
                .map((p)-> {PersonCountry personCountryName=new PersonCountry();
                personCountryName.setCountry(p.getCountry());
                return personCountryName; })
                .distinct()
                .forEach(System.out::println);
        System.out.println("*******************************************");
        //收集
        System.out.println("收集");
        personList.stream()
                .filter(p->p.getSex()=='F')
                .map(Person::getName)
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }


}
