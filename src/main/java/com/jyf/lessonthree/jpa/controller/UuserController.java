package com.jyf.lessonthree.jpa.controller;

import com.jyf.lessonthree.jpa.model.Uuser;
import com.jyf.lessonthree.jpa.service.UuserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Optional;

@Api(tags = {"用户信息查询"})
@RestController
@RequestMapping(name = "uuser",produces = {MediaType.APPLICATION_JSON_VALUE})
public class UuserController {
    @Resource
    private UuserService uuserService;

    @GetMapping("/UUid/{id}")
    @ApiOperation(value = "单个用户查询")
    public Optional<Uuser> getById(
            @ApiParam(required = true,value = "用户id",example = "1")
            @PathVariable Integer id
    ){
        Optional<Uuser> uuser = uuserService.getUuser(id);


        return uuser;



    }
}
