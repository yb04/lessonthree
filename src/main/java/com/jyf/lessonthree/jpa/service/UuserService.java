package com.jyf.lessonthree.jpa.service;

import com.jyf.lessonthree.jpa.model.Uuser;
import com.jyf.lessonthree.jpa.repository.UuserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UuserService {
    @Resource
    private UuserRepository uuserRepository;

     public Optional<Uuser> getUuser(Integer id){
         return  uuserRepository.findById(id);
     }
}
