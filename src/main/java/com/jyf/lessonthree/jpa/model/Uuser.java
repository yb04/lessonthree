package com.jyf.lessonthree.jpa.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "tbl_user")
public class Uuser {
    @Id
    @ApiModelProperty(value = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @ApiModelProperty(value = "last_name")
    @Column(name = "last_name",length = 50)
    private  String lastName;

    @ApiModelProperty(value = "fristName")
    @Column(name = "frist_Name",length = 50)
    private  String fristName;

    @ApiModelProperty(value = "邮箱")
    @Column(name = "email",length = 20) //省略默认列名就是属性名
    private String email;



}
