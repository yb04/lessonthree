package com.jyf.lessonthree.jpa.repository;

import com.jyf.lessonthree.jpa.model.Uuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UuserRepository extends JpaRepository<Uuser,Integer>, JpaSpecificationExecutor<Uuser> {

}
