package com.jyf.lessonthree;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.jyf"})
public class LessonthreeApplication {

    public static void main(String[] args) {
        SpringApplication.run(LessonthreeApplication.class, args);
    }



}
