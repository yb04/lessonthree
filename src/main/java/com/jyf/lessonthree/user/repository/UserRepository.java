package com.jyf.lessonthree.user.repository;

import com.jyf.lessonthree.user.model.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserModel,Integer>, JpaSpecificationExecutor<UserModel> {
   //通过是否在线查询
    List<UserModel> findByOnlinea(Integer onlinea);

    //通过id单个查询
   Optional<UserModel> findById(Integer id);

   //分页查询
    Page<UserModel> findAll(Pageable pageable);






}
