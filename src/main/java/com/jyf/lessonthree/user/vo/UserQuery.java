package com.jyf.lessonthree.user.vo;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "查询用于分页")
@Getter
@Setter
public class UserQuery  {
}
