package com.jyf.lessonthree.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "添加用户模型")
@Getter
@Setter
public class UserAdd {

    @ApiModelProperty(value = "用户名称")
    @NotBlank(message = "不能为空")
    private  String username;

    @ApiModelProperty(value = "用户年龄")
    @NonNull
    private Integer age;

    @ApiModelProperty(value = "用户地址")
    @NotBlank(message = "不能为空")
    private  String address;

    @ApiModelProperty(value = "是否在线",notes = "(0：在线，1：离线)")
    private  Integer onlinea;


}
