package com.jyf.lessonthree.user.service;

import com.jyf.lessonthree.user.model.UserModel;
import com.jyf.lessonthree.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {

    private UserRepository userRepository;
   //查询在线情况
   public List<UserModel> getListByOnlinea(Integer onlinea){
       return  userRepository.findByOnlinea(onlinea);
   }
   //单个查询根据id不知道getOne(id);什么玩意
    public Optional<UserModel> getById(Integer id){
       return  userRepository.findById(id);
    }

    public void save(UserModel userModel) {
        userRepository.save(userModel);
    }
}
