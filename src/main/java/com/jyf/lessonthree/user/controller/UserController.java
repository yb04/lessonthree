package com.jyf.lessonthree.user.controller;

import com.jyf.lessonthree.user.model.UserModel;
import com.jyf.lessonthree.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RestController
@Api(tags = "用户管理模块",description = "desrcition user")
@RequestMapping(value = "/user",produces = {MediaType.APPLICATION_JSON_VALUE})
public class UserController {
    @Resource
    private UserService userService;

    @GetMapping("/onlin/{onlinea}")
    @ApiOperation(value = "根据在线情况查询")
    public List<UserModel> getOnline(@ApiParam(required = true,value = "在线onlinea",example = "1")
                                     @PathVariable Integer onlinea
                                     ){
        return userService.getListByOnlinea(onlinea);
    }

    @ApiOperation(value = "根据id查询用户")
    @GetMapping("/userid/{id}")
    public Optional<UserModel> getUser(
           @ApiParam(required = true,value = "用户id",example = "1")
           @PathVariable Integer id
    ){
        return  userService.getById(id);
    }

    @GetMapping("getInfo")
    @ApiOperation(value = "获取商品列表信息")
    public String getProductInfo() {

        return   "测试信息";

    }



}
