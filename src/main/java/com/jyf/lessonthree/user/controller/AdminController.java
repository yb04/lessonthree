package com.jyf.lessonthree.user.controller;

import com.jyf.lessonthree.user.model.UserModel;
import com.jyf.lessonthree.user.service.UserService;
import com.jyf.lessonthree.user.vo.UserAdd;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@Api(tags = "用户管理添加模块",description = "desrcition user")
@RequestMapping(value = "user_upAnddet",produces = {MediaType.APPLICATION_JSON_VALUE})
public class AdminController {
   @Resource
   private UserService userService;

    @ApiOperation(value = "添加角色")
    @PostMapping
   public void addUser(@RequestBody @Valid UserAdd userAdd){
      UserModel userModel= new UserModel();

      userModel.setUsername(userAdd.getUsername());
       userModel.setAddress(userAdd.getAddress());
       userModel.setAge(userAdd.getAge());
       userModel.setOnlinea(userAdd.getOnlinea());

       userService.save(userModel);
   }


}
