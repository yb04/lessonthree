package com.jyf.lessonthree.user.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "t_user")
@Getter
@Setter
@Accessors(chain = true)
public class UserModel  implements Serializable {
    private static final long serialVersionUID = 1L;
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(columnDefinition = "int(11) COMMENT '用户id，自动生成'")
@ApiModelProperty(value = "id")
private  Integer id;


@ApiModelProperty(value = "用户名称")
@Column(length = 32,columnDefinition = "varchar(32)  COMMENT '用户姓名'",nullable=false)
private String username;

@ApiModelProperty(value = "用户年龄")
@Column(columnDefinition = "int(11) COMMENT '用户年龄'")
private Integer age;

@ApiModelProperty(value = "用户地址")
@Column(length = 50,columnDefinition ="varchar(50) COMMENT '用户地址'",nullable = true)
private  String address;

@ApiModelProperty(value = "是否在线",notes = "(0：在线，1：离线)")
@Column(columnDefinition = "tinyint(2) COMMENT '是否在线 (0：在线，1：离线)'")
private  Integer onlinea;



}
